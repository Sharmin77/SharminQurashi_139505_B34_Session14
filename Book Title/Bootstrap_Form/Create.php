<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../Asset/bootstrap/css/bootstrap.min.css">
    <script src="../../Asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../Asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Vertical (basic) form</h2>
    <form>
        <div style="color: #46b8da" class="form-group">
            <label for="email">Email:</label>
            <input style="border-color: magenta; background-color: lightpink"type="email" class="form-control" id="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Enter password">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn-lg btn-primary">Submit</button>
    </form>
</div>

</body>
</html>
